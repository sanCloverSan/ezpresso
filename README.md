# Ezpresso

## Name

Ezpresso

## Description

Project to learn Node.js, Expres.js MongoDB, automation testing using Mocha, validation/assertion using Chai. All the library and framework are being run on docker, thus, this project is also about learning how to config Docker.

## Installation

To run this project, make sure Docker is installed on your PC.

Copy either `.env.dev` or `.env.prod` to `.env` and fill the necessary configuration

Then, change to `./backend` directory.

Run `npm install` to install dependecies required for project.

If you are working in `development` environment run also `npm install --only=dev`.

After all the dependencies installed, run command below:

```docker
docker-compose up
```

If you want the container to have name other than `backend`, run `docker-compose` with `-p` switch:

```docker
docker-compose -p <container-name> up
```

If you want it to be rebuilt (apply changes in Dockerfile or docker-compose.yml):

```docker
docker-compose -p <container-name> up --build
```

## Usage

After running `docker-compose`, you can access the API at `localhost:8000` or `localhost:<WEB_PORT>` (WEB_PORT value configured in the `.env`). You can also access the `mongo-express` (web interface for MongoDB service) at  `localhost:<MONGO_WEB_PORT>`. Or you can connect using MongoDB Compass (installation required) using connection string `mongodb://<MONGO_INITDB_ROOT_USERNAME>:<MONGO_INITDB_ROOT_PASSWORD>@localhost:<MONGO_PORT>/<ME_CONFIG_MONGODB_AUTH_DATABASE>`. All these values are configured in the `.env`. Since MongoDB Compass is not connected in the docker, you have to manually change the values. For example, with this `.env`:

```env
MONGO_INITDB_ROOT_USERNAME=admin_007
MONGO_INITDB_ROOT_PASSWORD=Quantum0fS0l4c3
MONGO_PORT=27017
ME_CONFIG_MONGODB_AUTH_DATABASE=agents
```

The connection string for MongoDB Compass will be:

```text
mongodb://admin_007:Quantum0fS0l4c3@localhost:27017/agents
```

## License

[`ISC`](https://choosealicense.com/licenses/isc/)
