import { access, constants, open, writeFile, close } from 'node:fs';
import { generateKeyPairSync } from 'crypto';

function PKIInit() {
    const PRIVATE_KEY_PASSPHRASE = process.env.PRIVATE_KEY_PASSPHRASE;
    const { publicKey, privateKey, } = generateKeyPairSync('rsa', {
        modulusLength: 4096,
        publicKeyEncoding: {
            type: 'spki',
            format: 'pem'
        },
        privateKeyEncoding: {
            type: 'pkcs8',
            format: 'pem',
            cipher: 'aes-256-cbc',
            passphrase: PRIVATE_KEY_PASSPHRASE
        }
    });

    if (publicKey && privateKey) {
        const parrentDir = "/app/backend/config/"
        let pubKeyFileName = "public.key.pem";
        let pubKeyFilePath = parrentDir + pubKeyFileName;
        let privKeyFileName = "private.key";
        let privKeyFilePath = parrentDir + privKeyFileName;

        let pubKeyFileExist = access(pubKeyFilePath, constants.F_OK, (err) => {
            if (err) {
                console.log("Public key file not Exist!");
                return false;
            } else {
                return true;
            }
        });

        if (!pubKeyFileExist) {
            console.log("Creating file " + pubKeyFileName + " in " + parrentDir);
            open(pubKeyFilePath, "wx", (err, file) => {

                if (err) {
                    if (err.code === 'EEXIST') {
                        console.error(pubKeyFileName + ' already exists');
                        return;
                    }
                }

                console.log("Successfully creating " + pubKeyFileName);

                try {
                    let buffer = Buffer.from(publicKey);
                    writeFile(file, buffer, (err) => {
                        if (err) {
                            console.log(err);
                            throw err;
                        }
                    });

                    console.log("Successfully writing generated public key to " + privKeyFileName);
                } catch (err) {
                    console.log(err)
                    throw err;
                } finally {
                    close(file, (err) => {
                        if (err) {
                            throw err;
                        }
                    })
                }

            });
        }

        let privKeyFileExist = access(privKeyFilePath, constants.F_OK, (err) => {
            if (err) {
                console.log("Public key file not Exist!");
                return false;
            } else {
                return true;
            }
        });

        if (!privKeyFileExist) {
            console.log("Creating file " + privKeyFileName + " in " + parrentDir);
            open(privKeyFilePath, "wx", (err, file) => {

                if (err) {
                    if (err.code === 'EEXIST') {
                        console.error(privKeyFileName + ' already exists');
                        return;
                    }
                }

                console.log("Successfully creating " + privKeyFileName);

                try {
                    let buffer = Buffer.from(privateKey);
                    writeFile(file, buffer, (err) => {
                        if (err) {
                            console.log(err);
                            throw err;
                        }
                    });
                    console.log("Successfully writing generated private key to " + privKeyFileName);
                } catch (err) {
                    console.log(err)
                    throw err;
                } finally {
                    close(file, (err) => {
                        if (err) {
                            throw err;
                        }
                    })
                }
            });
        }
    }
}

export default PKIInit;