import SchemaUserInit from "./01_DBSchema_User_Init.js";
import PKIInit from "./02_Init_PubKey_PrivKey.js";
import fs from "fs";


export default function migrate() {
    if (!isMigrated()) {
        try {
            console.log("################# STARTING MIGRATION #################\n");

            //Migration
            console.log("################# GENERATING INITIAL USER SCHEMA #################\n");
            SchemaUserInit();

            console.log("################# GENERATING PUBLIC KEY, PRIVATE KEY #################\n");
            PKIInit();

            console.log("################# GENERATING MIGRATION HISTORY #################\n");
            //Generateing migration history
            generateHistory();

        } catch (err) {
            console.log(err)
            return false;
        }
    } else {
        console.log("Migration had already been done!");
    }
    return true;
}

function isMigrated() {
    let historyExist = fs.existsSync("./.migrate.json");
    console.log("Is migrated:", historyExist);
    return historyExist;
}

function generateHistory() {
    const history = {
        lastRun: Date.now(),
        migrations: {
            title: "Initial migrations",
            timestamp: Date.now(),
        }
    };
    fs.writeFileSync("./.migrate.json", JSON.stringify(history));
}


