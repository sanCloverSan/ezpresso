import UserSchema from "../model/User.js";
import bcryptjs from "bcryptjs";
import DBConnection from "../model/DBConnection.js";

const SchemaUserInit = async function () {

    const dbObj = new DBConnection();

    try {
        const dbConn = dbObj.connect();
        let User = dbConn.model("User", UserSchema);

        dbConn.on("error", (err) => {
            console.log("Error", err);
        })

        dbConn.once("open", function () {
            console.log("Connected successfully to Database");
        });

        const user1 = new User({ username: "john_doe", email: "john_doe@gmail.com", password: bcryptjs.hashSync("somehardpassword", bcryptjs.genSaltSync(10)) });

        try {
            await user1.save().then((err) => {
                if (err) {
                    console.log(err);
                }
                console.log("user John Doe added to db")
            });
        } catch (err) {
            console.log("user John Doe already exist");
        }

        const user2 = new User({ username: "jane_doe", email: "jane_doe@gmail.com", password: bcryptjs.hashSync("notsomehardpassword", bcryptjs.genSaltSync(10)) });
        try {
            await user2.save().then((err) => {
                if (err) {
                    console.log(err);
                }
                console.log("user Jane Doe added to db")
            });
        } catch (err) {
            console.log("user Jane Doe already exist");
        }
    } catch (err) {
        console.log(err);
    } finally {
        dbObj.close();
    }
}

export default SchemaUserInit;