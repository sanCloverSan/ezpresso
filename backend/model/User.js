import mongoose from "mongoose";

const { Schema } = mongoose;

const UserSchema = new Schema({
  username: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  }
}, { emitIndexErrors: true })

const handleE11000 = function(error, res, next) {
  if (error.name === 'MongoError' && error.code === 11000) {
    next(new Error('There was a duplicate key error'));
  } else {
    next();
  }
};

// UserSchema.post('save', handleE11000);
// UserSchema.post('update', handleE11000);
// UserSchema.post('findOneAndUpdate', handleE11000);
// UserSchema.post('insertMany', handleE11000);

export default UserSchema;
