import mongoose from "mongoose";

class DBConnection {
    _connectionString = "";
    _connection = null;
    
    constructor() {
        const mongoUsername = process.env.MONGO_INITDB_ROOT_USERNAME;
        const mongoUserPassword = process.env.MONGO_INITDB_ROOT_PASSWORD;
        const mongoDBServer = process.env.ME_CONFIG_MONGODB_SERVER;
        const mongoPort = process.env.MONGO_PORT;
        const mongoDBName = process.env.MONGO_INITDB_DATABASE;

        this._connectionString = `mongodb://${mongoUsername}:${mongoUserPassword}@${mongoDBServer}:${mongoPort}/${mongoDBName}`;
    }

    connect() {
        const mongoUsername = process.env.MONGO_INITDB_ROOT_USERNAME;
        const mongoUserPassword = process.env.MONGO_INITDB_ROOT_PASSWORD;
        const mongoDBAuth = process.env.ME_CONFIG_MONGODB_AUTH_DATABASE;
        this._connection = mongoose.createConnection(this._connectionString, {
            authSource: mongoDBAuth,
            user: mongoUsername,
            pass: mongoUserPassword,
            useUnifiedTopology: true
        });
        return this._connection;
    }

    close() {
        if (this._connection) this._connection.close();
    }
}

export default DBConnection;