
import BasicController from "/app/backend/controller/BasicController.js";

function indexRouter(app) {
    app.get('/api/v1', BasicController.home);
}


export default indexRouter;
