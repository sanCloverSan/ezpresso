import MigrationController from "../controller/MigrationController.js";
function migrationRoute(app) {
    app.get("/-/migrate", MigrationController.migration);
}

export default migrationRoute;

