import LoginController from "/app/backend/controller/LoginController.js";

function loginRoute(app) {
    app.post('/api/v1/login', LoginController.login);
}


export default loginRoute;
