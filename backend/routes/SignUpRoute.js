import verifySignUp from "../middleware/verifySignUp.js";
import SignUpController from "../controller/SignUpController.js";

function signUpRoute(app) {
    app.post("/api/v1/signup", verifySignUp.checkDuplicateUsernameOrEmail, SignUpController.signup);
}

export default signUpRoute;

