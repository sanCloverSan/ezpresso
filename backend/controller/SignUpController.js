import bcryptjs from "bcryptjs";
import DBConnection from "../model/DBConnection.js";
import UserSchema from "../model/User.js";

const SignUpController = {
    async signup(req, res) {
        const username = req.body.username;
        const email = req.body.email;
        const password = req.body.password;

        if (!username) {
            res.send({
                status: false,
                message: "Username must be included"
            });
        }

        if (!email) {
            res.send({
                status: false,
                message: "Email must be included"
            });
        }

        if (!password) {
            res.send({
                status: false,
                message: "Password must be included"
            });
        }

        if (username && password && email) {
            const db = new DBConnection();
            try {
                const dbConn = db.connect();
                let User = dbConn.model("User", UserSchema);

                let newUser = new User({
                    username: username,
                    email: email,
                    password: bcryptjs.hashSync(password, bcryptjs.genSaltSync(10))
                })

                let err, queryResult = await newUser.save();

                if (err) {
                    throw new Error(err);
                }

                if (!queryResult) {
                    return res.status(500).send({
                        status: false,
                        message: "Something unexpected happened."
                    });
                }

                res.send({
                    status: true,
                    message: "Succesfully signed up!"
                });

            } catch (err) {
                return res.status(500).send({
                    status: false,
                    message: err
                });
            } finally {
                db.close();
            }
        }
    }
}

export default SignUpController;