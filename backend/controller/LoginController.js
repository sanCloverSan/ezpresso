import bcryptjs from "bcryptjs";
import jwt from "jsonwebtoken";
import fs from "fs";
import DBConnection from "../model/DBConnection.js";
import UserSchema from "../model/User.js";


const LoginController = {
    async login(req, res) {

        const username = req.body.username;
        const password = req.body.password;

        if (!username) {
            res.send({
                status: false,
                message: "Username must be included"
            });
        }

        if (!password) {
            res.send({
                status: false,
                message: "Password must be included"
            });
        }

        if (username && password) {
            const db = new DBConnection();

            try {
                const dbConn = db.connect();
                let User = dbConn.model("User", UserSchema);

                let err, user = await User.findOne({ $or: [{ username: username }, { email: username }] });
                if (err) {
                    console.log(err);
                    throw new Error(err);
                }                

                if (!user) {
                    return res.status(400).send({
                        status: false,
                        message: "Username or email and password combination doesn't match!"
                    })
                }else{
                    const passwordHash = user.password;

                    let match = bcryptjs.compareSync(password, passwordHash);

                    if(match){
                        const  privateKey =  fs.readFileSync(process.env.PRIVATE_KEY_PATH);
                        const privateSecret = {
                            key: privateKey, 
                            passphrase: process.env.PRIVATE_KEY_PASSPHRASE
                        };

                        const signOptions = {
                            algorithm: 'RS256',
                            expiresIn: '1h'
                          }

                        const token = jwt.sign({username: username}, privateSecret, signOptions );
                        const cookieOptions = {
                            maxAge: 1000 * 60 * 60, // would expire after 60 minutes
                            httpOnly: true, // The cookie only accessible by the web server
                            secure: true
                        }
                        res.cookie("auth_token", token, cookieOptions);
                        return res.status(200).send({
                            status: true,
                            message: "Succesfully logged in!",
                            token: token
                        })
                    }else{
                        return res.status(400).send({
                            status: false,
                            message: "Username or email and password combination doesn't match!"
                        })
                    }
                }

            } catch (err) {
                return res.status(500).send({
                    status: false,
                    error: err.message,
                    message: err
                });
            } finally {
                db.close();
            }
        }
    }
}

export default LoginController;