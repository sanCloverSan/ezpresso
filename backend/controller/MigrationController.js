import migrate from "../migration/00_Init_Migration.js";
const MigrationController = {
    migration: (req, res) => {
        const isMigrated = migrate();
        res.send(isMigrated ? "Migration success!" : "Migration failed!");
    }
}

export default MigrationController;