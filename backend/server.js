import express from "express";
import bodyParser from "body-parser";
import helmet from "helmet";

import SignUpRouter from "./routes/SignUpRoute.js"
import LoginRouter from "./routes/LoginRoute.js"
import IndexRouter from "./routes/IndexRoute.js"
import MigrationRoute from "./routes/MigrationRoute.js"

const app = express();
const PORT = process.env.WEB_PORT || 4000;

// Parse JSON bodies for this app
app.use(express.json());
// Use helmet for safety ;)
app.use(helmet());
// Hide x-powered-by:express
app.disable("x-powered-by");
app.use(bodyParser.urlencoded({ extended: false }));

//Routes
IndexRouter(app);
LoginRouter(app);
SignUpRouter(app);
MigrationRoute(app);

// custom 404
app.use((req, res, next) => {
  res.status(404).send("Sorry can't find that!")
})

// custom error handler
app.use((err, req, res, next) => {
  console.error(err.stack)
  res.status(500).send('Something broke!')
})

app.listen(PORT, () => {
  console.log(`Success! Your application is running on port ${PORT}.`)
})