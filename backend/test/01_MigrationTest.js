import { assert } from "chai";
import UserSchema from "../model/User.js";
import DBConnection from "../model/DBConnection.js";


const connObj = new DBConnection();
const connection = connObj.connect();

let User = connection.model("User", UserSchema);

describe('User', function () {

  it('should return John Doe\'s username', function (done) {
    User.findOne({ username: "john_doe" }, (err, res) => {
      if (err) done(err);
      assert(res !== "undefined", "John is undefined");
      assert.equal(res.username, "john_doe");
      done();
    });
  });

  it('should return Jane Doe\'s username', function (done) {
    User.findOne({ username: "jane_doe" }, (err, res) => {
      if (err) done(err);
      assert(res !== "undefined", "Jane is undefined");
      assert.equal(res.username, "jane_doe");
      connObj.close();
      done();
    });
  });
});





