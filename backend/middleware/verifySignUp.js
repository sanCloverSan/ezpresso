import UserSchema from "../model/User.js";
import DBConnection from "../model/DBConnection.js";

const checkDuplicateUsernameOrEmail = async (req, res, next) => {
    const db = new DBConnection();
    try {
        const dbConn = db.connect();
        let User = dbConn.model("User", UserSchema);

        dbConn.on("error", (err) => {
            res.status(500).send({
                message: err
            });
        })

        let username = req.body.username;
        let email = req.body.email;

        if (username) {
            let err, user = await User.findOne({ username: username });
            if (err) {
                console.log(err)
                return res.status(500).send({
                    status: false,
                    message: err
                });
            }

            if (user) {
                return res.status(400).send({
                    statu: false,
                    message: "Username already taken!"
                })
            }

        } else {
            return res.status(400).send({
                statu: false,
                message: "Username must be included!"
            });
        }

        if (email) {
            let err, emailResult = await User.findOne({ email: email });
            if (err) {
                return res.status(500).send({
                    statu: false,
                    message: err
                });
            }

            if (emailResult) {
                return res.status(400).send({
                    statu: false,
                    message: "Email already in use!"
                })
            }
        } else {
            return res.status(400).send({
                statu: false,
                message: "Email must be included!"
            });
        }
        next();
    } catch (err) {
        return res.status(500).send({
            statu: false,
            message: err
        });
    } finally {
        db.close();
    }
}

const verifySignUp = {
    checkDuplicateUsernameOrEmail
}

export default verifySignUp;